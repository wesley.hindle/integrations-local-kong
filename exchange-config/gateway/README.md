# Gateway documents

# `default-config.yaml`

Used to determine how this product will be configured in the Gateway.

## Services

[What is a service?](https://docs.konghq.com/gateway/3.0.x/get-started/services-and-routes/#what-is-a-service)
- `name` (required) The name of the service
- `url` (required) The URL of the service, must be a valid URL
- `timeout`:
  - `secs` (required) The number of seconds before a timeout occurs, can be a number between 1-900
  - `retries` (required) The number of retries to execute upon failure, can be a number between 0-10
- `tls_verify_upstream` (optional) Determines whether auth is enabled, can be 'yes/no/on/off/true/false' Defaults to true.
- `addtional_ca_certificates` (optional) A list of additional CA certificates for mutual auth (self-signed certificates) 

e.g.

```yaml
services:
  - name: "example"
    url: "https://httpbin.org:443" 
    timeout:
      secs: 30
      retries: 1
    tls_verify_upstream: false 
    additional_ca_certificates: "4e3ad2e4-0bc4-4638-8e34-c84a417ba39b"
```

## Routes

[What is a route?](https://docs.konghq.com/gateway/3.0.x/get-started/services-and-routes/#what-is-a-route)
- `routes` (required)
  - `host` (required) The domain name of the host machine offering the service
  - `consumer_path` (required) The consumer path for the service
  - `http_methods` (required) The methods available in the service as a string of comma separated values

e.g.

```yaml
services:
  // Ommitted for brevity
  routes:
    - host: localhost
      consumer_path: "/consumer-path"
      http_methods: "GET,HEAD,POST,PUT,OPTIONS"
```


## Plugins
[What are plugins?](https://docs.konghq.com/hub/plugins/overview/#what-are-plugins)

[Plugin catalogue](https://docs.konghq.com/hub/)

- `plugins` (optional) can be at service or root level
  - `rate_limiting` (optional)  All consumers on the service / route will share the rate-limit defined. It should be defined in a way to protect backend services which cannot easily scale with high throughput.
    - `per_second` (optional) Number of requests that can be made per second
    - `per_minute` (optional) Number of requests that can be made per minute
    - `per_hour` (optional) The number of requests per hour
    - `per_day` (optional) The number of requests per day


  - `correlationId` (optional) A unique ID for tracing a request throughout the tech stack on its upstream journey, The format is correlationId:<headerKey> where headerKey is up to the user to choose what header name they want to store the correlationId in.
  - The following fields allow you to transform the requests before they reach the upstream server
  - `request_transform_headers` (optional) 
  - `request_transform_body` (optional)
  - `request_transform_querystring` (optional)
  - The fields below are the methods used for the transformation, it's fine to apply multiple e.g. Add and Replace
    - `add` (optional) Add will add {key:value} only if {key} not present
    - `append` (optional) Append will add {key:value} alongside any {key:existingValue}
    - `remove` (optional) Remove will remove {key:existingValue}
    - `rename` (optional) Rename will change {existingkey:existingValue} to {key:existingValue}
    - `replace` (optional) Replace will update {key:existingValue} to {key:value} only if {key} is present
  - `oidc_apigw` (optional) *Details at [wiki](https://dwpdigital.atlassian.net/l/cp/19n5FU4u)* 
    - `discovery_endpoint` (required) The discovery endpoint on DTH or Azure AD
    - `client_id` (required) Provided by ID&T during the initial onboarding 
    - `scopes_required` (required) The scopes required to be present in the access token
    - `issuers_allowed` (required) The issuers allowed to be present in the tokens
    - `client_secret` (optional) The client secret.
  - `oidc_websso`(optional) *Details at [wiki](https://dwpdigital.atlassian.net/wiki/spaces/AS/pages/132822041183/Exchange+Interface+-+Plugin+oidc-websso)*
    - `oidc_websso.discovery_endpoint` (required) The discovery endpoint on DTH or Azure AD
    - `client_id` (required) Provided by ID&T during the initial onboarding
    - `client_secret` (optional) The client secret.


    >Redis is required as backend for this plugin. Default values can be overridden as below:
    ```yaml
     redis:
      server_name:  <Server name indicatior for connecting the Redis server- defaults to server>
      host: <Redis host - defaults to 127.0.0.1 >
      port: <Redis port - defaults to 6379>
      ssl: <Use SSL/TLS for Redis connection - defaults to false>
      ssl_verify: <Verify Redis server cert - defaults to false>
      connect_timeout: <Redis connection timeout in ms - defaults to 60000>
      read_timeout: <Redis read timeout in ms - defaults to 10000>
      send_timeout: <Redis send timeout in ms - defaults to 10000>
      database: <Database to use - defaults to 0>
    ```

### Advanced plugins

If you wish to add a plugin which is not currently supported by the publishing tools, you can do so by adding the `kongfig` key to your plugins configuration and adding the plugin as part of that key (see kongfig example below)

## Examples

### Standard

```
services:
  - name: "example"
    url: "https://httpbin.org:443" #can view upstream request in browser http://localhost:8000/consumer-path/get
    timeout:
      secs: 30 
      retries: 1 
    tls_verify_upstream: true 
    routes:
      - host: localhost
        consumer_path: "/consumer-path"
        http_methods: "GET,HEAD,POST,PUT,OPTIONS"
        plugins:
          - rate_limiting:
              per_minute: 20 
              per_second: 5
          - correlationId: "trackingId"
```
### Extended
```yaml
services:
  - name: "example"
    url: "https://httpbin.org:443" #can view upstream request in browser http://localhost:8000/consumer-path/get
    timeout:
      secs: 30 
      retries: 1 
    tls_verify_upstream: true 
    additional_ca_certificates: "4e3ad2e4-0bc4-4638-8e34-c84a417ba39b"
    routes:
      - host: localhost
        consumer_path: "/consumer-path"
        http_methods: "GET,HEAD,POST,PUT,OPTIONS"
        plugins:
          - rate_limiting:
              per_minute: 20 
              per_second: 5
          - correlationId: "trackingId"
          - request_transform_headers:
              replace:
                - systemId:696
              add:
                - X-CertificateOuid:myId 
          - request_transform_body:
              append:
                - my_key:my_value 
              rename:
                - originalKeyName1:newKeyName1
                - originalKeyName2:newKeyName2
          - request_transform_querystring:
              remove:
                - argOneInQueryString
                - argTwoInQueryString
    plugins:
      - oidc_websso:
          discovery_endpoint: http://test-me
          client_id: test-client
```
### Kongfig

```
services:
  - name: "trying"
    url: "https://httpbin.org:443" #can view upstream request in browser http://localhost:8000/consumer-path/get
    timeout:
      secs: 30
      retries: 1
    tls_verify_upstream: true
    routes:
      - host: "dev.exchange.dwp.gov.uk"
        consumer_path: "/consumer-path"
        http_methods: "GET,HEAD,POST,PUT,OPTIONS"
        plugins:
          - rate_limiting:
              per_minute: 15
              per_second: 10
          - correlationId: "trackingId"
          - kongfig:
              - name: example_plugin
                config:
                  example_config1: example1
                  example_config2: example2
                  example_config3: example3
          - oidc_websso:
              discovery_endpoint: http://test-me
              client_id: test-client
              session:
                redis:
                  prefix: my-session
                  port: 99891
                  ssl: true
                  ssl_verify: true
    plugins:              
      - oidc_apigw:
        discovery_endpoint: http://test-me
        client_id: test-client
        scopes_required:
          - email
          - name
          - role
        issuers_allowed:
          - dwp
          - gov
        client_secret:
          - secret_for_client_1
          - secret_for_client_2
```