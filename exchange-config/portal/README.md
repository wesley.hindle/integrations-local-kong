# Portal documents

# `meta.yaml`
This contains the `workspace` and `product_name` which combine to form a unique identifier for the product.
Workspace will be agreed and setup with Integration.

# `catalogue-entry.yaml`

Used to determine how this product appears in the Portal Catalogue.

- `description` (required)
- `primaryContact` (required) Contact details for the owner of the asset, i.e. the Product Owner.
    These details will be used when customers make enquiries or request access to the asset through the portal.
  * name (optional)
  * emailAddress (required) must end with `gov.uk`
  * role (optional)
- `technical_documentation` (optional) Allows you to provide paths to technical documentation to be published to the portal.
The following options are supported, and the path provided should be relative to the current working directory:
  * `oas_path`
  * `wsdl_path`
- `filters` A list of filtering options to improve searchability on the portal.
  * asset_type:
    * REST API
    * SOAP API
    * Event
    * Datasets
  * owner: 
    * AME Payments
    * Citizen Income
    * Citizen Information
    * Document Repository Services
    * Integration
    * Legacy Bridge
    * Notifications Platform
    * Personal Independence Payment
    * Reference Data Service
    * Retirement, Bereavement & Care
    * Other
  * lifecycle - select one value, without the definition:
    * In Development - Conceptual, evolving specification
    * Pre-Prod - Built, documented and defined and available to use in test instance
    * Prod - Live
    * Deprecated - In use but no new customers allowed
    * Removed - Obsolete
  * profile - select one value, without the definition:
    * Strategic - Aligns to SRA
    * Transitional - Bridge between Strategic and Legacy. Not intended for long term use
    * Legacy - Not aligned to SRA
  * access - select one value, without the definition:
    * Open - No restrictions to use
    * Closed - Closed community
  * visibility: 
    * Internal
    * Public
  * categories - one to many of:
    * Address
    * Awards
    * Citizen
    * Data
    * Document Services
    * Financial
    * GUID
    * SMS
    * Other

  Additional filter information can be found here: https://dwpdigital.atlassian.net/wiki/spaces/AS/pages/132550656149/Portal+Filters

- `searchTags` (required) Tags to label the asset in order to increase discoverability. Minimum of 3.
Tags must be in csv format:
  * Javascript, Node.js, Web Development
- `relatedAssets` (optional) A list of other assets that are related to the asset being published.
e.g. an Event could add the API that it consumes. Must match the Asset ID on the related asset's
business documentation page on the portal.

## Filter combinations

The `lifecycle`, `profile`, and `access` values must be one of the below combinations:

| Lifecycle      | Profile      | Access |
|----------------|--------------|--------|
| In Development | Strategic    | Open   |
| In Development | Strategic    | Closed |
| In Development | Transitional | Open   |
| In Development | Transitional | Closed |
| Pre-Prod       | Strategic    | Open   |
| Pre-Prod       | Strategic    | Closed |
| Pre-Prod       | Transitional | Open   |
| Pre-Prod       | Transitional | Closed |
| Prod           | Strategic    | Open   |
| Prod           | Strategic    | Closed |
| Prod           | Transitional | Open   |
| Prod           | Transitional | Closed |
| Prod           | Legacy       | Closed |
| Deprecated     | Legacy       | Closed |
| Removed        | Legacy       | Closed |

# `business-documentation.md`

Add information describing your product to help consumers decide whether it is suitable for their needs.

## Headings

Headings can be defined with any heading level, using '#' or multiple '#' to denote headers.

There is certain validation around the recommended headings below.

Additional custom headings can also be added.

## Recommended Headings

### `Overview` (required)

Providing a full description (in non-technical language) of the asset on the portal. For instance:

- What is the asset?
- Why was it developed?

### `Use Cases`

Provide details on some of the use cases where the asset is used, or could be used.

### `Data Provided`  (required)

What data is provided by the asset(s). Please include what you get access to, breaking it down into every possible field / data feed a user can access. 

Please include references to versions and what’s included in each clearly. 

Note: Yes, your technical documentation will explain this. The aim is to make it easy for a non-technical user (who may not wish to search through intimidating language / code) to discover.

### `Known Issues and Limitations`

Provide details of any known issues or limitations regarding the asset. 

For instance, frequently asked questions and/or what you aren’t getting access to (but might believe you will).

### `Useful Resources`

Provide additional resources, tools or templates to help users accelerate adopting the asset.

### `Getting Started`

Provide details on how to get started in using the asset. Please include anything the user must provide to gain access, an idea of timescales etc. 

### `Support` (required)

Provide support details - including a point of contact (Product Owner, Lead Engineer), contact details, what support is available / not available etc.

### `License Information`

Insert DWP license information. This should be the SPDX identifier for the license (if applicable). 

### `Further Info`

Provide any further information that isn’t included in the above sections.

### Release Notes

Any additional information of note in particular in relation to release version



